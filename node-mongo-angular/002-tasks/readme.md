# Prerequisites
- Install mongodb. When writing the tutorial I used 3.2. You will need to adopt it to the version you install.
- Install chrome
- Install postman in chrome via the chrome marketplace. 

# Add task model

- Create a folder models
- Create  a file called task-model.js

``` javascript
const mongoose = require('mongoose');

var TaskSchema = mongoose.Schema({
    title: String,
    description: String,
    date: Date,
    completed: Boolean
});

var Task = mongoose.model('Task',TaskSchema );
module.exports = Task;
```

# Add task service that can list, add, update tasks

- Create a folder services
- Create a file called task-service.js

``` javascript
// import the model functionality
const Task = require('../models/task-model');

// define the service interface
module.exports = {
    list: function (done) {
    },

    add: function (title, done) {
    },

    update: function (id, updatedFields, done) {
    }
};
```

## Implement the service functions. 
**Note**: 
- Mongoose can either return a promise or use a done callback. In our implementation we use the promise version but we'll have a done callback which is provided from the controller.
- The functions are async, if you return data they will not do anything. The handlers should provide the done callback with the `(err,data)=>{}` signature.


``` javascript
list: function (done) {
    // Query, this is where you can define how to find things
    Task.find({})
        //strip not needed mongoose stuff 
        .lean()
        // run and return a promise
        .exec()
        // handle success/failure
        .then(mongoResponse => {
            done(null, mongoResponse);
        }, (err) => {
            return done(err);
        });
},
```

When saving a task mongoose will return the database object. This new object will have the `_id` property which is the built in database id of the newly saved document.

``` javascript
 add: function (title, done) {
     // create a new Task object
    var task = new Task({
        title: title,
        date: Date.now(),
        completed: false,
        description: ''
    });

    // save
    task.save()
        // handle success/failure
        .then(savedTask => {
            done(null, savedTask);
        }, (err) => {
            return done(err);
        });
},
```

To update an existing document you need to provide a query that find the document, then specify the fields that need updating along with their new value as a JS object. e.g.
`{'name': 'New Name'}`

``` javascript
update: function (id, updatedFields, done) {
    // find the instance and define what needs updating
    Task.update({ '_id': id }, updatedFields)
        // handle success/failure
        .then(updated => {
            done(null, updated);
        }, (err) => {
            done(err);
        });
}
```

# Initialise mongoose and import the service into index.js

Add this to the top of the index.js file, right below the existing require statements.

``` javascript
const mongoose = require('mongoose');
const taskService = require('./services/task-service.js');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/test', {useMongoClient: true});
```

# Add route handlers for task/list, task/add, task/update

Extend the `index.js` file to have some route handlers. Route handlers paths and http requests with code that handles them and responds with either data, view or status codes.
There's a whole universe of functionality to learn here but to keep it brief we'll simply set up some post routes.

The idea is that for all routes (except for the main view) the handlers will send a 500 status code if there was any error and a valid response otherwise.

Please note how body parser auto processes the req (request) body and all we need to do is to access the JS object properties.

The handlers simply take the data from the request, perform some basic sanity checking and calla the service we implemented earlier.


## List tasks

``` javascript

app.post('/task/list', (req, res) => {
    taskService.list((err, response) => {
        if (err) {
            return res.sendStatus(500);
        }
        res.json(response);
    });
});

```

##  Add a task

``` javascript
app.post('/task/add', (req, res) => {
    var title = req.body.title || 'New task';

    taskService.add(title, (err, response) => {
        if (err) {
            return res.sendStatus(500);
        }
        res.json(response);
    });
});

```


##  Update a task

``` javascript

app.post('/task/update', (req, res) => {
    var id = req.body.id || null;
    if (!id) {
        return res.sendStatus(500);
    }

    var update = req.body.update || null;
    if (!update) {
        return res.sendStatus(500);
    }

    taskService.update(id, update, (err, response) => {
        if (err) {
            return res.sendStatus(500);
        }
        res.json(response);
    });
});

```

# Start the app
- Start the app the same way as you did in 001
- Make sure Mongo is running. If you have mongo 3.2 installed and you are doing this on windows you can use thge start-mongo.cmd file to run the database server. If you have a different mongo version isntalled modify the cmd file to look in the right folder. 
```
@echo off
md .\data
call "C:\Program Files\MongoDB\Server\3.2\bin\mongod.exe" --dbpath data
```

# Test the new routes using postman


- Start chrome 
- Start postman
  - Click apps in the bookmarks bar
  - Click postman

## Listing tasks 

- In Postman set the following

```
Method: Post
Url: http://localhost:3000/task/list
```

- Click Send
- The response should be

``` javascript
[]
```
## Adding a Task

- In Postman set the following

```
Method: Post
Url: localhost:3000/task/add
Body content type: JSON (application/json)
Body:
{
    "title": "Hello World Task"
}
```
- Click Send

- The response should be something like this

``` json
{
  "__v": 0,
  "title": "Hello World Task",
  "date": "2016-08-24T20:13:58.558Z",
  "completed": false,
  "description": "",
  "_id": "57be000659f5474418edea6c"
}
```

- At this point if you list the tasks ( select the previous call from the history view on the left) you should see the new task in the returned collection.

## Updating a Task

- In Postman set the following

```
Method: Post
Url: localhost:3000/task/update
Body content type: JSON (application/json)
Body:
{ 
    // make sure this ID matches the task's that you just created
    "id": "57be000659f5474418edea6c",
    "update":{
        "title": "Hello World Task12"
    }
}
```
- Click Send

- The response should be something like this

``` json
{
  "ok": 1,
  "nModified": 1,
  "n": 1
}
```

- At this point if you list the tasks you should see the new name.