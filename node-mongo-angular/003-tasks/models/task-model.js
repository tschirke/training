const mongoose = require('mongoose');

var TaskSchema = mongoose.Schema({
    title: String,
    description: String,
    date: Date,
    completed: Boolean
});

var Task = mongoose.model('Task',TaskSchema );
module.exports = Task;