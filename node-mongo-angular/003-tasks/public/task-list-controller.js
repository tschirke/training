var TaskListController = ['$http', '$rootScope', function ($http, $rootScope) {
    var self = this;
    self.tasks = [];

    self.list = function () {
        $http.post('/task/list').then(function (response) {
            var data = response.data;
            self.tasks = data;
        }).catch(function (err) {
            console.log("Err: ", err);
        });
    };

    self.addTask = function () {
        $http.post('/task/add')
            .then(function (response) {
                var data = response.data;
                self.tasks.unshift(data);
            }, function (err) {
                console.log("Err: ", err);
            });
    };

    self.selectTask = function (task) {
        $rootScope.$broadcast('task.select', task);
    };

    self.removeTask = function (task) {
        $http.post('/task/remove', { id: task._id })
            .then(function (response) {
                var index = self.tasks.indexOf(task);
                self.tasks.splice(index, 1);
            }, function (err) {
                console.log("Err: ", err);
            });
    }
    self.list();
}];

exports.register = function (module) {
    module.controller('TaskListController', TaskListController);
};