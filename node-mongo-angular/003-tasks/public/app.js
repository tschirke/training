(function (angular) {
    var appName = 'demoApp';

    var TaskListController = require('/public/task-list-controller.js');

    var TaskDetailController = require('/public/task-detail-controller.js')

    var app = angular.module(appName, []);

    // TODO: Require application components and register them with the app
    TaskListController.register(app);
    TaskDetailController.register(app);

    angular.element(document).ready(function () {
        angular.bootstrap(document, [appName]);
    });

})(window.angular);