var TaskDetailController = ['$http', '$rootScope', function ($http, $rootScope) {
    var self = this;
    self.task = null;
    self.changed = false;

    $rootScope.$on('task.select', function (event, arg) {
        // Duplicate task to prevent live editing
        self.changed = false;
        if (self.task === arg) {
            self.task == null;
        } else {
            self.task = arg;
        }
    });

    self.change = function () {
        self.changed = true;
    };

    self.save = function () {
        var updateTime = Date.now();
        var dataToSend = {
            id: self.task._id,
            update: {
                title: self.task.title,
                description: self.task.description,
                completed: self.task.completed,
                date: updateTime
            }
        }

        $http.post('/task/update', dataToSend)
            .then(function (response) {
                if (response.data.ok == 1 && response.data.nModified === 1) {
                    self.task.date = updateTime;
                }
            },function (err) {
                console.log("Err: ", err);
            });
    }
}];

exports.register = function (module) {
    module.controller('TaskDetailController', TaskDetailController);
};