# Set jquery version to 2.2.4

By default when we installed jquery npm got the latest version. Unfortunately boostrap 3 only works with jquery version < 3. This is easy to fix. 

- Open the package.json file
- Update the jquery line to `"jquery": "2.2.4"`
- While at it make sure boostrap is referencing v3.x `"bootstrap": "3.3.7",`
- Open a command prompt in the same folder
- Run `npm i` to install the new dependencies

# Set up system js

System JS allows loading javscript files as modules similar to how nodeJS works. Using it allows for a similar workflow when working with frontend code.

- Install system js
  - Open a command prompt to the folder that contains `package.json`
  - Run `npm i systemjs --save`
- Add create the public/system-config.js file and add the following to it

``` javascript
SystemJS.config({
    baseURL: '/public',
    defaultExtension: 'js',
    format: 'cjs',
});

```

This tells the module loader to look treat /public as the root folder when specifying paths in the require statements, if no extensions are specified default to js and use the commonJS module format - basically this will make the code look like nodejs modules.

# Set up the layout pug file to include the required librarties and styles

Open the views/layout.pug file add the following to it.

``` pug
html
    head
        link(rel="stylesheet" href="/modules/bootstrap/dist/css/bootstrap.min.css" )
        link(rel="stylesheet" href="/modules/bootstrap/dist/css/bootstrap-theme.min.css" )

    body
        div.container
            block content
    script(src="/modules/jquery/dist/jquery.min.js")
    script(src="/modules/bootstrap/dist/js/bootstrap.min.js")
```

If you run the application now and open the main page in the browser you should see that the text is displayed with a different font and size. This is because boostrap took over the styling of the page.


# Bootstrap the angular app

- Open the views/layout.pug file add replace it with.

```
html
    head
        link(rel="stylesheet" href="/modules/bootstrap/dist/css/bootstrap.min.css" )
        link(rel="stylesheet" href="/modules/bootstrap/dist/css/bootstrap-theme.min.css" )

    body
        div.container
            block content
    script(src="/modules/jquery/dist/jquery.min.js")
    script(src="/modules/bootstrap/dist/js/bootstrap.min.js")
    script(src="/modules/angular/angular.js")
    script(src="/modules/systemjs/dist/system.src.js")
    script(src="/public/system-config.js")
    script
        | SystemJS.import('app.js');
        
```

The new script lines are the angular, systemjs, system-config references and the embedded script block that kicks systemjs into action by importing and running the app startup script called `app.js`

**Note** the `app.js` file is located in the public folder. The systemjs config defines the root folder to be /public  

- Create the app.js file in the public folder and set it to the  following

``` javascript
(function (angular) {
    var appName = 'demoApp';
    var app = angular.module(appName, []);

    // TODO: Require application components and register them with the app
    
    angular.element(document).ready(function () {
        angular.bootstrap(document, [appName]);
    });

})(window.angular);
```

This is a self invoking function. This script is executed when the page is loaded. It initialises the angular app and applies it to the whole document. This means the angular directives will be available on any node.

# Create a sample controller to verify if agular is working

This is a temporary change and will be reverted in the next step
- Update the app.js file 

``` javascript
(function (angular) {
    var appName = 'demoApp';
    var app = angular.module(appName, []);

    // TODO: Require application components and register them with the app

    app.controller('testController',[function(){
        var self = this;

        self.greet = 'Hello World';
    }]);
    
    angular.element(document).ready(function () {
        angular.bootstrap(document, [appName]);
    });

})(window.angular);

```

Update the index.pug file

```
extends layout

block content
    h1 Ultimate Task List
    div(ng-controller="testController as ctrl")
        h2 {{ctrl.greet}}
```

- Run the app and in the browser you should see Hello World in the second line on the main page.

- Remove the test controller and remove the new div from the layout file

# App design

The app will have the following major UI elements.

- Toolbar
  - Users can add new tasks by clicking on a toolbar button.
- Task list. Below the toolbar on the left. 
  - It will display the tasks
  - Tasks can be selected by cliking on their names
- Task details pane. When a task is selected an editor is displayed on the right 
  - Users can update task details and complete the tasks in there

# Create tasklistcontroller
Add a file called task-list-controller to /public with the following in it. 

``` javascript
var TaskListController = ['$http', '$rootScope', function ($http, $rootScope) {
    var self = this;
    self.tasks = [];

    // Gets the task list from the server
    self.list = function () {
        $http.post('/task/list').then(function (response) {
            var data = response.data;
            self.tasks = data;
        }, function (err) {
            console.log("Err: ", err);
        });
    };

    // add a new task to the database, and adds the returned instance to the top of the list 
    self.addTask = function () {
        $http.post('/task/add')
            .then(function (response) {
                var data = response.data;
                self.tasks.unshift(data);
            }, function (err) {
                console.log("Err: ", err);
            });
    };

    // performs task selection, it sends a message via $rootscope to any scubscriber. Will be important later.
    self.selectTask = function (task) {
        $rootScope.$broadcast('task.select', task);
    };

    // removes the specified task fromthe database and removes the instance from the list
    self.removeTask = function (task) {
        $http.post('/task/remove', { id: task._id })
            .then(function (response) {
                var index = self.tasks.indexOf(task);
                self.tasks.splice(index,1);
            }, function (err) {
                console.log("Err: ", err);
            });
    }

    // runs the list function whent he script executes (when angular creates an instance of thie component)
    self.list();
}];

// module exports a function that can be used to register its contents with the app
exports.register = function (module) {
    module.controller('TaskListController', TaskListController);
};
```

## Register the controller with the angular app
- Add the following to the `app.js` file

``` javascript
(function (angular) {
    var appName = 'demoApp';

    // NEW --
    var TaskListController = require('/public/task-list-controller.js');
    // NEW --

    var app = angular.module(appName, []);

    // TODO: Require application components and register them with the app
    
    // NEW --
    TaskListController.register(app);
    // NEW --

    angular.element(document).ready(function () {
        angular.bootstrap(document, [appName]);
    });

})(window.angular);
```

- Run the app.
- Nothing happens. The page is still the shame. It is because the page needs some angular markup to display the data and interact with it.

## Create the task list view

- Replace index.pug with the following

```
extends layout

block content
    h1 Ultimate Task List

    div.row
        div.col-xs-8(ng-controller="TaskListController as ctrl")
            div.row
                div.col-xs-12
                    h2 Task List
                    div.btn-toolbar
                        button.btn.btn-info.btn-sm(type="button" ng-click="ctrl.addTask()") Add New
            div.row
                div.col-xs-12
                    div.panel.panel-default(ng-repeat="task in ctrl.tasks" ng-class="{'panel-success': task.completed}")
                        div.panel-heading 
                            a(href="#" ng-click="ctrl.selectTask(task)") {{task.title}}
                            button.btn.btn-danger.btn-sm.pull-right(type="button" ng-click="ctrl.removeTask(task)") Delete
                            span.clearfix
                        div.panel-body
                            p
                                a(href="#" ng-click="ctrl.selectTask(task)") {{task.date | date : 'medium'}}
                            pre(ng-bind="task.description")

                    h3(ng-if="ctrl.tasks.length==0") The task list is empty. Try adding some...
                        
        div.col-xs-4
            div.row
```

- Run the app.
- You should see the test tasks listed along with a button. If you click the button new tasks will be added to the list. Also you can delete tasks by clicking the delete button.
- If you click the task title however nothing happens. That will be fixed  next   

# Create taskdetailcontroller

- Add a file in /public called task-detail-controller.js with the following contents

``` javascript

var TaskDetailController = ['$http', '$rootScope', function ($http, $rootScope) {
    var self = this;
    self.task = null;
    self.changed = false;

    // handle the task select message
    $rootScope.$on('task.select', function (event, arg) {
        // Duplicate task to prevent live editing
        self.changed = false;
        if (self.task === arg) {
            self.task == null;
        } else {
            self.task = arg;
        }
    });

    // function to be called when any of the task parameters change in the details editor
    self.change = function () {
        self.changed = true;
    };

    // saves the task to the database
    self.save = function () {
        var updateTime = Date.now();
        var dataToSend = {
            id: self.task._id,
            update: {
                title: self.task.title,
                description: self.task.description,
                completed: self.task.completed,
                date: updateTime
            }
        }

        $http.post('/task/update', dataToSend)
            .then(function (response) {
                if (response.data.ok == 1 && response.data.nModified === 1) {
                    self.task.date = updateTime;
                }
            }, function (err) {
                console.log("Err: ", err);
            });
    }
}];

// registers the controller with the angular app (module)
exports.register = function (module) {
    module.controller('TaskDetailController', TaskDetailController);
}; 

``` 

## Add the controller to the app

- Update the `app.js` file to include the new controller so it can be added to the DOM.

``` javascript
(function (angular) {
    var appName = 'demoApp';

    var TaskListController = require('/public/task-list-controller.js');
     
    // NEW --
    var TaskDetailController = require('/public/task-detail-controller.js')
    // NEW --


    var app = angular.module(appName, []);

    // TODO: Require application components and register them with the app
    TaskListController.register(app);

     // NEW --
    TaskDetailController.register(app);
     // NEW --

    angular.element(document).ready(function () {
        angular.bootstrap(document, [appName]);
    });

})(window.angular);
``` 

## Update the index page to include the editorin the second part


```
extends layout

block content
    h1 Ultimate Task List

    div.row
        div.col-xs-8(ng-controller="TaskListController as ctrl")
            div.row
                div.col-xs-12
                    h2 Task List
                    div.btn-toolbar
                        button.btn.btn-info.btn-sm(type="button" ng-click="ctrl.addTask()") Add New
            div.row
                div.col-xs-12
                    div.panel.panel-default(ng-repeat="task in ctrl.tasks" ng-class="{'panel-success': task.completed}")
                        div.panel-heading 
                            a(href="#" ng-click="ctrl.selectTask(task)") {{task.title}}
                            button.btn.btn-danger.btn-sm.pull-right(type="button" ng-click="ctrl.removeTask(task)") Delete
                            span.clearfix
                        div.panel-body
                            p
                                a(href="#" ng-click="ctrl.selectTask(task)") {{task.date | date : 'medium'}}
                            pre(ng-bind="task.description")

                    h3(ng-if="ctrl.tasks.length==0") The task list is empty. Try adding some...
                        
        div.col-xs-4
            div.row
                div.col-xs-12(ng-controller="TaskDetailController as ctrl")
                    h2 Details
                    div.col-xs-12
                        table.table.table-bordered(ng-if="ctrl.task")
                            tbody
                                tr
                                    td Title
                                    td
                                        input(type="text" ng-model="ctrl.task.title" ng-change="ctrl.change()")
                                tr
                                    td Description
                                    td
                                        textarea(type="text" ng-model="ctrl.task.description" ng-change="ctrl.change()")
                                tr
                                    td Completed
                                    td
                                        label
                                            input(type="checkbox" ng-model="ctrl.task.completed" ng-change="ctrl.change()")
                                            | {{ctrl.task.completed===true?"Completed":"In Progress"}}
                                    tr
                                        td 
                                            |  
                                        td
                                            button.btn.btn-success.btn-sm(type="button" ng-disabled="!ctrl.changed" ng-click="ctrl.save()") Save
                                        

```

- Run the app
- The change is that when you select a task by clicking on its title the new editor shows up
- If you change any parameters in the editor the task live updates on the screen
- If you click save the task will be saved to the database
- If you reload the page the saved task data is loaded from the database

# Conclusion

The goal was to introduce you to a few concepts and help with getting started using these frameworks for building better tools and web apps.

You have learned about

- Initialising a mongo app
- Using npm to install dependencies
- Setting up an express app with data and view endpoints
- Creating, querying and updating mongo database documents using mongoose
- Creating a pug (jade) layout and content page
- Initialising angular using systemjs to be able to author various components as modules
- Authoring controllers that talk to each other via messages and perform various async server opertations
- Testing endpoints using postman

I hope it was useful. 