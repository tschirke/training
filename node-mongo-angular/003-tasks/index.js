

const express = require('express');
const bodyParser = require('body-parser')
const path = require('path');
const app = express();
const mongoose = require('mongoose');
const taskService = require('./services/task-service.js');
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost/test', {useMongoClient: true});
app.set('port', process.env.PORT || 3000)
app.use(bodyParser.json())

app.use('/modules', express.static(path.join(__dirname, '/node_modules')));
app.use('/public', express.static(path.join(__dirname, '/public')));

app.set('view engine', 'pug');

app.get('/', (req, res) => {
    res.render('index')
})

app.post('/task/list', (req, res) => {
    taskService.list((err, response) => {
        if (err) {
            return res.sendStatus(500);
        }
        res.json(response);
    });
});


app.post('/task/add', (req, res) => {
    var title = req.body.title || 'New task';

    taskService.add(title, (err, response) => {
        if (err) {
            return res.sendStatus(500);
        }
        res.json(response);
    });
});



app.post('/task/update', (req, res) => {
    var id = req.body.id || null;
    if (!id) {
        return res.sendStatus(500);
    }

    var update = req.body.update || null;
    if (!update) {
        return res.sendStatus(500);
    }
    if (!update.date) {
        update.date = Date.now();
    }

    taskService.update(id, update, (err, response) => {
        if (err) {
            return res.sendStatus(500);
        }
        res.json(response);
    });
});


app.post('/task/remove', (req, res) => {
   var id = req.body.id || null;
   if(!id){
       return res.sendStatus(500);
   }

    taskService.remove(id, (err, response) => {
        if (err) {
            return res.sendStatus(500);
        }
        res.json(response);
    });
});

app.listen(app.get('port'), () => {
    console.log("Web server listening on port " + app.get('port'));
});