var Task = require('../models/task-model');


module.exports = {
    list: function (done) {
        Task.find()
            .sort("-date")
            .lean()
            .exec()
            .then(mongoResponse => {
                done(null, mongoResponse);
            }, (err) => {
                return done(err);
            });
    },

    add: function (title, done) {
        var task = new Task({
            title: title,
            date: Date.now(),
            completed: false,
            description: ''
        });

        task.save()
            .then(savedTask => {
                done(null, savedTask);
            }, (err) => {
                return done(err);
            });
    },

    update: function (id, updatedFields, done) {
        Task.update({ '_id': id }, updatedFields)
            .then(updated => {
                done(null, updated);
            }, (err) => {
                done(err);
            });
    },

    remove: function (id, done) {
        Task.find({ '_id': id })
            .remove()
            .exec()
            .then(updated => {
                done(null, updated);
            },(err) => {
                done(err);
            });
    }
};