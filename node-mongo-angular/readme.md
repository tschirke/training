# Node-Express-Mongo-Angular training

These folders contain a quick intro to creating a todo app in nodejs, express and angular 1. 
The application is bare bones and the training was designed to help beginners to grasp what they can do with the technologies rather than advising on best practices for development (testing, pacakging, security, etc...).
That would be another course.

Please note completing the training will take about 8 hours. 


# Instructions

## Prerequisites 
- NodeJS 8 (LTS)
- MongoDb 3.6

Make sure mongodb is running before starting.

## How to follow
Create a new directory somewhere, open it in Visual Studio Code (or the code editor of your choice) from the directory.

Open the readme.md file in the 001-tasks follow the instructions in the folder you have created.

The 001-tasks folder already contains the result of all the steps so if you get stuck you'll have somewhere to refer to.

Once 001 is completed your code will be in a state where 002 starts. Follow the instructions in the readme.md file there. The folder contains the code for the completed step.

Once 002 is completed your code will be in a state where 003 starts. Follow the instructions in the readme.md file there. The folder contains the code for the completed step.

You have finished the training.






