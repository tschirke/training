# Initialise node app
```
npm init -y
```

# Install required libraries

```
npm i mongoose express pug bootstrap angular jquery body-parser --save
```

- **mongoose** - mongodb object modelling tool
- **express** - web development framework
- **pug** - view engine (template engine)
- **boostrap** - responsive front end framework
- **angular** - MVVM framework
- **jquery** - boostrap dependency, we will not use this
- **body-parser** - allows parsing request payload from  various formats into js objects, we'll use the json one.

# Setup index.js

``` javascript
console.log("Hello world");
```

# Add public and views folder

- Create  a folder called public

Public is where the static files, app scripts, will be served from

# Set up layout.pug and index.pug

- Create a folder called /views and add the follwing files to it

layout.pug - this file is the page shell that provides common page layout for your app

```
html
    head
    body
        block content
```

index.pug - this file contains the page that gets rendered as theindex page

```
extends layout

block content
    h1 Hello World

```

# Configure express

- Add express, body parser, path references. 
- Create an app instance
- Add two static folders: /public and /node_modules
- Enable json request bodies
- Set port
- Set view engine to 'pug'
- Start the express server

``` javascript


const express = require('express');
const bodyParser = require('body-parser')
const path = require('path');
const  app = express();

app.use('/modules', express.static(path.join(__dirname, '/node_modules')));
app.use('/public', express.static(path.join(__dirname, '/public')));


app.set('port', process.env.PORT || 3000)
app.use(bodyParser.json()) //parses json, multi-part (file), url-encoded
app.set('view engine', 'pug');

app.get('/', (req, res) => {
    res.render('index')
})


app.listen(app.get('port'), function(){
  console.log("Web server listening on port " + app.get('port'));
});
```

# Start the app

- **command line** - In VS code right click on the left pane (explorer) and select  `Open command prompt` in there type `node index` and ENTER
- **from VS Code** - press F5. Close the debugger parameter json file and press F5 again. You only need to deal with this json file once.
- Open Chrome and type `http://localhost:3000/` into the address bar. You should see the rendered index page.