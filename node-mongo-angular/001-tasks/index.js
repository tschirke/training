

const express = require('express');
const bodyParser = require('body-parser')
const path = require('path');
const  app = express();

app.use('/modules', express.static(path.join(__dirname, '/node_modules')));
app.use('/public', express.static(path.join(__dirname, '/public')));


app.set('port', process.env.PORT || 3000)
app.use(bodyParser.json()) //parses json, multi-part (file), url-encoded
app.set('view engine', 'pug');

app.get('/', (req, res) => {
    res.render('index')
})


app.listen(app.get('port'), function(){
  console.log("Web server listening on port " + app.get('port'));
});